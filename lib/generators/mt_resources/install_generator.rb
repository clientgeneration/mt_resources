module MtResources
  module Generators
    class InstallGenerator < Rails::Generators::Base
        # this line defines the source path, defaults to parent app
        source_root File.expand_path("../../templates", __FILE__)

        def add_route
            route "mount MtResources::Engine => '/library'"
        end

        def create_initializer_file
            copy_file '../../templates/mt_resources.rb', 'config/initializers/mt_resources.rb'
        end

        def create_note_model
            if yes?("This gem requires resource model, create one now?")
                generate "model", "resource title:string description:text resource_type:string resource_file:string file_name:string file_size:string file_type:string file_extension:string account:references created_by:integer updated_by:integer created_ip:string updated_ip:string"
            else
                puts "\033[31m\tPlease create one manually or the gem wont work.\033[0m"
            end
        end

        # def active_record_default
        #     insert_into_file "app/models/note.rb", :after => "class Note < ActiveRecord::Base\n" do
        #         "  before_save :default_values\n " +
        #         "  def default_values\n    self.title = title.presence || 'untitled'\n  end\n"
        #     end
        # end

        def create_decorator
            copy_file '../../templates/resource_decorator.rb', 'app/decorators/resource_decorator.rb'
        end

        def add_to_account
            insert_into_file "app/models/account.rb", :after => "class Account < ActiveRecord::Base\n" do
                "  has_many :resources\n "
            end
        end

        def add_to_tenant
            insert_into_file "app/models/tenant.rb", :after => "class Tenant\n" do
                "  def resources\n    account.resources\n  end\n"
            end
        end

        # append_to_file 'config/environments/test.rb' do
        #   'config.gem "rspec"'
        # end

        def requirements
            puts "\n============================================================"
            puts "Requirements"
            puts "============================================================"
            puts "1. Must have current_tenant, user and account defined"
            puts "2. Ensure that current_tenant has resources \n\n"
            puts "3. Create carrierwave uploader file \n\n"
        end
    end
  end
end