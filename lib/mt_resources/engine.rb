# require 'Filesize'

module MtResources
  class Engine < ::Rails::Engine
    isolate_namespace MtResources
  end

  # these code sets variable from initializer
  class << self
      mattr_accessor :mt_resources_layout_name, :mt_resources_allowed_extensions
      self.mt_resources_layout_name = "application"
      self.mt_resources_allowed_extensions = []
      # add default values of more config vars here
  end

   # this function maps the vars from your app into your engine
   def self.setup(&block)
      yield self
   end

end
