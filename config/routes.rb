MtResources::Engine.routes.draw do
	resources :resources, :path => "library" do
		collection do
			get "images_url"
			post "froala_image_upload"
		end
	end
end
