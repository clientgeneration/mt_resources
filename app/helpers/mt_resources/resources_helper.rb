module MtResources
  module ResourcesHelper
  	def file_icon resource_type
  		if resource_type == "image"
  			"<i class='fa fa-file-image-o fa-fw colorize'></i>"
  		elsif resource_type == "audio"
  			"<i class='fa fa-file-audio-o fa-fw colorize'></i>"
  		elsif resource_type == "video"
  			"<i class='fa fa-file-video-o fa-fw colorize'></i>"
  		elsif resource_type == "pdf"
  			"<i class='fa fa-file-pdf-o fa-fw colorize'></i>"
  		elsif resource_type == "excel"
  			"<i class='fa fa-file-excel-o fa-fw colorize'></i>"
  		elsif resource_type == "powerpoint"
  			"<i class='fa fa-file-powerpoint-o fa-fw colorize'></i>"
  		elsif resource_type == "word"
  			"<i class='fa fa-file-word-o fa-fw colorize'></i>"
  		elsif resource_type == "text"
  			"<i class='fa fa-file-text-o fa-fw colorize'></i>"
  		else
  			"<i class='fa fa-file-o fa-fw colorize'></i>"
  		end
  	end

  	def file_size bytes
  		return Filesize.from("#{bytes} B").pretty
  	end
  end
end
