# use default parent application_controller
require_dependency "/application_controller"

module MtResources
  class ResourcesController < ApplicationController
  	before_action :authenticate_user!, except: ["images_url", "froala_image_upload"]
  	layout MtResources.mt_resources_layout_name

    def index
    	@resource = current_tenant.resources.new
    	@resources = current_tenant.resources.order(created_at: :desc).decorate

    	respond_to do |format|
			format.html { render 'mt_resources/resources/index' }
			format.js { render "/mt_resources/resources/remote/index" }
		end
    end

    def create
    	@resource = current_tenant.resources.new(resource_params)
		if @resource.save 
			respond_to do |format|
				format.html { redirect_to resources_path, notice: 'Upload successful.'  }
				format.js { redirect_to resources_path, format: 'js' }
			end
		else
			# doesnt work
			render action: "index"
		end
    end

    def destroy
		@resource = current_tenant.resources.find(params[:id])
		@resource.destroy
		redirect_to resources_path, notice: 'File deleted.'
	end

	# for use with froala editor choose image
	def images_url
		@resources = current_tenant.resources.where(resource_type: "image").order(created_at: :desc)
		images_url = []
		@resources.each do |resource|
			images_url << resource.resource_file.url
		end
		render layout: false, :text => images_url
	end

	# for use with froala editor to upload image
	def froala_image_upload
		@resource = current_tenant.resources.new(resource_file: params[:file], account_id: current_user.account_id, 
			created_by: current_user.id, created_ip: request.remote_ip, updated_by: current_user.id, updated_ip: request.remote_ip)

		if @resource.save 
			image_url = {link: @resource.resource_file.url}
			render layout: false, :json => image_url
		else
			render :nothing => true
		end
	end

    private
	def resource_params
		if params[:action] == 'create'
			params[:resource][:account_id]=current_user.account_id
			params[:resource][:created_by]=current_user.id
			params[:resource][:created_ip]=request.remote_ip
		end
		params[:resource][:updated_by]=current_user.id
		params[:resource][:updated_ip]=request.remote_ip
		params.require(:resource).permit(:resource_file, :account_id, :created_by, :created_ip, :updated_by, :updated_ip)  
	end
  end
end
