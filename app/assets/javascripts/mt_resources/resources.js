// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

function resourcesInit(){

	if ($('#new-resource').length > 0){
		if ($('#new-resource').data( "allowed-type" ).length > 0) {
		  	var whiteListTypesRegex = new RegExp("(\.|\/)("+$('#new-resource').data( "allowed-type" ).join("|")+")$", "i");
		  	$('#new-resource').fileupload({
				dataType: "script",
				dropZone: $('#upload-file-dropzone'),
				add: function(e, data) {
					var file = data.files[0];
					if (whiteListTypesRegex.test(file.type) || whiteListTypesRegex.test(file.name)){
						data.context = $(tmpl("template-upload", file));
						$('.resource-file-progress-container').append(data.context);
						return data.submit();
					}else{
						alert(file.name+" is not an accepted format \n\nAccepted files are: "+ $('#new-resource').data( "allowed-type" ).join(", "))
					}
				},
				progress: function(e, data) {
					var progress;
					if (data.context) {
						progress = parseInt(data.loaded / data.total * 100, 10);
						data.context.find('.progress-bar').html(progress + '%');
						return data.context.find('.progress-bar').css('width', progress + '%');
					}
				}
			});
		}else{
			$('#new-resource').fileupload({
				dataType: "script",
				dropZone: $('#upload-file-dropzone'),
				add: function(e, data) {
					var file = data.files[0];
					data.context = $(tmpl("template-upload", file));
					$('.resource-file-progress-container').append(data.context);
					return data.submit();
				},
				progress: function(e, data) {
					var progress;
					if (data.context) {
						progress = parseInt(data.loaded / data.total * 100, 10);
						data.context.find('.progress-bar').html(progress + '%');
						return data.context.find('.progress-bar').css('width', progress + '%');
					}
				}
			});
		}
	}



	

	$('#upload-file-dropzone').find('#select-file-button').click(function() {  
		$('#resource-file-input').click();
	});

};

$(document).ready(resourcesInit);
$(document).on('page:load', resourcesInit);




// $('#new_painting').fileupload({
//     dataType: "script",
//     add: function(e, data) {
//       var file, types;
//       types = /(\.|\/)(gif|jpe?g|png)$/i;
//       file = data.files[0];
//       if (types.test(file.type) || types.test(file.name)) {
//         data.context = $(tmpl("template-upload", file));
//         $('#new_painting').append(data.context);
//         return data.submit();
//       } else {
//         return alert("" + file.name + " is not a gif, jpeg, or png image file");
//       }
//     },
//     progress: function(e, data) {
//       var progress;
//       if (data.context) {
//         progress = parseInt(data.loaded / data.total * 100, 10);
//         return data.context.find('.bar').css('width', progress + '%');
//       }
//     }
//   });
